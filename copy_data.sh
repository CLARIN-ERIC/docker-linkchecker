#!/bin/bash

# shellcheck source=copy_data.env.sh
source "${DATA_ENV_FILE:-copy_data.env.sh}"


init_data (){
	LINKCHECKER_GIT_DIRECTORY="$(pwd)/linkchecker-src"
    
    cleanup_data

    if [ -z "${DEV_LOCATION}" ]
    then     
	mkdir -p "${LINKCHECKER_GIT_DIRECTORY}"
        (
	  		TMP_TGZ=$(mktemp)
			cd "${LINKCHECKER_GIT_DIRECTORY}" && 
				wget -O "${TMP_TGZ}" "${LINKCHECKER_REPO_URL}/archive/${LINKCHECKER_BRANCH}.tar.gz" &&
				tar zxvf "${TMP_TGZ}" --strip-components 1 &&
				rm "${TMP_TGZ}"
        )
    else
        cp -r "$DEV_LOCATION" "$CURATION_GIT_DIRECTORY"
    fi  
}

cleanup_data () {
    if [ -d "${LINKCHECKER_GIT_DIRECTORY}" ]; then
    	echo "Cleaning up source directory ${LINKCHECKER_GIT_DIRECTORY}"
	    rm -rf "${LINKCHECKER_GIT_DIRECTORY}"
	fi
}
