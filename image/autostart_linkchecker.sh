#!/bin/bash
#
# author: Wolfgang Walter SAUER (wowasa) <clarin@wowasa.com>
# the script is triggered by supervisord on start. If the environment variable AUTOSTART_LINKCHECKER=true it's loading and starting the
# linkchecker topology immediately

if [ "$AUTOSTART_LINKCHECKER" = true ] ; then
   "${STORM_DIRECTORY}/bin/storm" jar "${LINKCHECKER_DIRECTORY}/linkchecker.jar" org.apache.storm.flux.Flux -e -r -R "${LINKCHECKER_FLUX_FILE}"
fi