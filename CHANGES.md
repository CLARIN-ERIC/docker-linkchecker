# version 4.0.1_2.1.0
- upgrading to linkchecker 4.0.1
- upgrading base image, storm and zookeeper

# version 4.0.0_2.0.0
- upgrade to Java 21
- upgrading apache storm to version 2.6.3

# version 3.0.0_1.1.0
- upgrade to Java 17
- using project maven wrapper instead of alpine maven package
- using environment variables in crawler-conf.yaml instead of replacement by init.sh (init.sh removed)
 
# version 2.1.2
- dropping start-cluster.sh script
- starting cluster (zookeeper, storm-nimus, storm-supervisor) by supervisor (configuration in image/supervisor/storm.conf)
- fluentd configuration for log-files of zookepper, storm-nimbus, storm-supervisor and storm-worker
- taking standard log4j configuration for zookeeper and storm with minor modifications (done by sed in Dockerfile)


# version 2.1.1
- changes in [linkchecker](https://github.com/clarin-eric/linkchecker/blob/master/CHANGES.md)

# version 2.1.0
- changes in [linkchecker](https://github.com/clarin-eric/linkchecker/blob/master/CHANGES.md)
- upgrading zookeeper to version 3.7.0 and storm to version 2.2.0
- adding packages gcompat and libstdc++ to image to prevent crash of storm nimbus (in Dockerfile)
- adding storm.yaml with worker.childopts to image to prevent unintended termination of workers (in Dockerfile)
- adding worker.xml with log4j configuration for storm-worker
- renaming all occurrences of stormychecker to linkchecker
- setting linkchecker topology in inactive state at loading time (in start-cluster.sh)  

